<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Organization Summary Add Form</title>
</head>
<body>

<form action="store.php" method="post">
    Please Enter Organization Name:
    <input type="text" name="orgName">
    <br>
    Please Enter Type:
    <input type="text" name="orgType">
    <br>
    Please Enter Owner Name:
    <input type="text" name="ownerName">
    <br>
    Please Enter Foundation Time:
    <input type="date" name="foundationTime">
    <br>
    Please Enter Mission:
    <input type="text" name="orgMission">
    <br>

    <input type="submit">

</form>

<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>