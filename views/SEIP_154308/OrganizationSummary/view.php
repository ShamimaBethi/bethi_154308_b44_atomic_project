<?php
require_once("../../../vendor/autoload.php");

$objOrganizationSummary = new \App\OrganizationSummary\OrganizationSummary();
$objOrganizationSummary->setData($_GET);
$oneData = $objOrganizationSummary->view();


?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Organization Summary - Single Organization Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center" ;">Organization Summary - Single Organization Information</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <<th>Organization Name</th>
            <th>Type</th>
            <th>Owner Name</th>
            <th>Founded</th>
            <th>Mission</th>
            <th>Action Buttons</th>
        </tr>

        <?php

        echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->org_name</td>
                     <td>$oneData->type</td>
                     <td>$oneData->owner_name</td>
                     <td>$oneData->founding_time</td>
                     <td>$oneData->mission</td>

                     <td><a href='index.php' class='btn btn-info'>Back To Active List</a> </td>
                  </tr>
              ";

        ?>

    </table>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>